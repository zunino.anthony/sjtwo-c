// Edited by Anthony Zunino 9/3/22

#include <stdio.h>

typedef struct {
  float f1; // 4 bytes
  char c1;  // 1 byte
  float f2;
  char c2;
} __attribute__((packed)) my_s;

int main(void) {
  printf("Hello world!\n");

  my_s s;

  // TODO: Instantiate a struct of type my_s with the name of "s"
  printf("Size : %d bytes\n"
         "floats 0x%p 0x%p\n"
         "chars  0x%p 0x%p\n",
         sizeof(s), &s.f1, &s.f2, &s.c1, &s.c2);
  /*
      Seems like without the "packed" the struct saves all types as
      the same size of 4 bytes for each of the four pieces of data (16 unpacked),
      and as their actual size if packed (10 packed). Doing some research on the matter,
      there is something called data allignment which has the computer read/write in
      to word sized chunks of 4 bytes using padding for a 32-bit system. Using attribute
      packed will not allow the padding and bring down the total data to 10 bytes.
  */
  return 0;
}